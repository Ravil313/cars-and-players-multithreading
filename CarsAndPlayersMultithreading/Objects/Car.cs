﻿using System.Collections.Generic;

namespace CarsAndPlayersMultithreading.Objects
{
    /// <summary>
    /// Класс автомобиля:
    /// название
    /// координата автомобиля
    /// игрок - текущий водитель
    /// список игроков-пассажиров 
    /// </summary>
    internal class Car
    {
        public string CarName { get; set; }
        public Сoordinate CarСoordinate { get; set; }
        public Player Driver { get; set; }
        public ICollection<Player> Passengers { get; set; }
    }
}
