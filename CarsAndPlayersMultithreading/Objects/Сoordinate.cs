﻿using System;

namespace CarsAndPlayersMultithreading.Objects
{
    /// <summary>
    /// Класс координаты:
    /// int X и Y
    /// метод для определения расстояния между координатами
    /// </summary>
    internal class Сoordinate
    {
        public int X { get; set; }
        public int Y { get; set; }
        public double GetDistance(int xA, int xB, int yA, int yB)
        {
            var distance = Math.Sqrt((Math.Pow(xB,2)-Math.Pow(xA,2)) + (Math.Pow(yB, 2) - Math.Pow(yA, 2)));
            return distance;
        }
    }
}
