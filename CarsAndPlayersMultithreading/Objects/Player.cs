﻿namespace CarsAndPlayersMultithreading.Objects
{
    /// <summary>
    /// Класс игрока:
    /// никнейм
    /// координата игрока
    /// </summary>
    internal class Player
    {
        public string PlayerName { get; set; }
        public Сoordinate PlayerCoordinate { get; set; }
    }
}
