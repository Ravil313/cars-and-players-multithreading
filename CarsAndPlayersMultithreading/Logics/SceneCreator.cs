﻿using System;
using System.Collections.Generic;
using System.Threading;
using CarsAndPlayersMultithreading.Objects;

namespace CarsAndPlayersMultithreading.Logics
{
    internal class SceneCreator
    {
        private const int playersCount = 1000;
        private const int carsCount = 200;
        private const int mapSize = 99;
        private int coordinatesCount = playersCount + carsCount;        
        private Random _rnd = new Random();
        private List<Сoordinate> coordList;
        private List<Player> playersList;
        private List<Car> carsList;

        /// <summary>
        /// Создать общие списки игроков и автомобилей,
        /// заполнить случайными данными (1000 игроков и 200 автомобилей, координаты [0, 100]).
        /// </summary>
        public SceneCreator()
        {
            coordList = CreateCordinates();
            playersList = CreatePlayers();
            carsList = CreateCars();
        }
        private List<Сoordinate> CreateCordinates()
        {            
            List<Сoordinate> coordinates = new List<Сoordinate>()
            {
                new Сoordinate
                {
                    X = _rnd.Next(1, mapSize),
                    Y = _rnd.Next(1, mapSize)
                }
            };
            while (coordinates.Count < coordinatesCount)
            {
                var _coordinate = new Сoordinate
                {
                    X = _rnd.Next(1, mapSize),
                    Y = _rnd.Next(1, mapSize)
                };
                foreach (var coordinate in coordinates)
                {
                    if (coordinate.X != _coordinate.X && coordinate.Y != _coordinate.Y)
                    {
                        coordinates.Add(_coordinate);
                        break;
                    }
                }
            }
            return coordinates;            
        }
        private List<Player> CreatePlayers()
        {            
            var players = new List<Player>();
            for (int i = 0; i < playersCount; i++)
            {
                var rndIndex = _rnd.Next(0, coordList.Count);
                var newPlayer = new Player
                {
                    PlayerName = $"Player {i}",
                    PlayerCoordinate = coordList[rndIndex]
                };
                players.Add(newPlayer);
                coordList.RemoveAt(rndIndex);
            }
            return players;
        }
        private List<Car> CreateCars()
        {
            var cars = new List<Car>();
            for (int i = 0; i < carsCount; i++)
            {
                var rndIndex = _rnd.Next(0, coordList.Count);
                var newCar = new Car
                {
                    CarName = $"Car {i}",
                    CarСoordinate = coordList[rndIndex],
                    Passengers = new List<Player>()
                };
                cars.Add(newCar);
                coordList.RemoveAt(rndIndex);
            }
            return cars;
        }
        /// <summary>Заполнение машин игроками</summary>
        public void SeatingPlayersInCars()
        {            
            for (int i = 0; i < carsList.Count; i++)
            {
                var rndIndex = _rnd.Next(0, playersList.Count);
                playersList[rndIndex].PlayerCoordinate = carsList[i].CarСoordinate;
                carsList[i].Driver = playersList[rndIndex];
                playersList.RemoveAt(rndIndex);
                while (carsList[i].Passengers.Count < 3) 
                {
                    var rndIndex2 = _rnd.Next(0, playersList.Count);
                    var passengerCandidate = playersList[rndIndex2];
                    if (carsList[i].CarСoordinate != passengerCandidate.PlayerCoordinate)
                    {
                        passengerCandidate.PlayerCoordinate = carsList[i].CarСoordinate;
                        carsList[i].Passengers.Add(passengerCandidate);
                        playersList.RemoveAt(rndIndex2);
                    }                        
                }
            }
            Thread.Sleep(4000);
        }
        /// <summary>Печать результата</summary>
        public void PrintResult()
        {
            //в основном потоке вывести информацию о 5 случайных автомобилях:
            for (int i = 0; i < 5; i++)
            {
                var rndIndex1 = _rnd.Next(0, carsList.Count);
                Console.Write($"Автомобиль: {carsList[rndIndex1].CarName} координаты: X:{carsList[rndIndex1].CarСoordinate.X}, Y:{carsList[rndIndex1].CarСoordinate.Y} ");
                Console.WriteLine();
                Console.Write($"Водитель: {carsList[rndIndex1].Driver?.PlayerName} координаты: X:{carsList[rndIndex1].Driver?.PlayerCoordinate.X}, " +
                    $"Y:{carsList[rndIndex1].Driver?.PlayerCoordinate.Y}");
                Console.WriteLine();
                foreach (var passenger in carsList[rndIndex1].Passengers)
                {
                    Console.Write($"Пассажир:{passenger?.PlayerName}, координаты: X:{passenger?.PlayerCoordinate.X}, " +
                        $"Y:{passenger?.PlayerCoordinate.Y}");
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            //выбрать случайную машину и вывести всех игроков в радиусе 15 от нее в формате "имя игрока: расстояние":
            var randomCar = carsList[_rnd.Next(0, carsList.Count)];
            Console.WriteLine($"Автомобиль: {randomCar.CarName} координаты: X:{randomCar.CarСoordinate.X}, Y:{randomCar.CarСoordinate.Y} ");
            foreach (var player in playersList)
            {
                var distance = player.PlayerCoordinate.GetDistance(player.PlayerCoordinate.X, randomCar.CarСoordinate.X, player.PlayerCoordinate.Y, randomCar.CarСoordinate.Y);
                if (distance <= 15)
                {
                    Console.WriteLine($"Имя игока: {player.PlayerName}, расстояние: {distance}");
                }
            }
            Console.ReadLine();
        }
    }
}
