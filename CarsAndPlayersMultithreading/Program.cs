﻿using System;
using System.Threading;
using CarsAndPlayersMultithreading.Logics;
using CarsAndPlayersMultithreading.Objects;

namespace CarsAndPlayersMultithreading
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SceneCreator scene = new SceneCreator(); //создание списков игроков и автомобилей
            Thread t = new Thread(new ThreadStart(scene.SeatingPlayersInCars)) { IsBackground = true }; //заполнение авто игроками в фоновом потоке
            t.Start(); //запуск фонового потока
            t.Join(); //синхронизация потоков
            scene.PrintResult(); //вывод результата
        }
    }
}
